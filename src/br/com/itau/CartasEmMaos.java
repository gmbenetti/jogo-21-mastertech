package br.com.itau;

import java.util.ArrayList;

public class CartasEmMaos implements Cartas {

    private  ArrayList<Carta> cartasEmMaos = new ArrayList<Carta>();
    private int pontos = 0;

    @Override
    public int getQuantidadeCartas() {
        return this.cartasEmMaos.size();
    }

    @Override
    public void listaCartas() {
        System.out.println("\n       Você tem " + getQuantidadeCartas() + " em mãos:");
        for(int i = 0; i < cartasEmMaos.size(); i++){
            System.out.println("         (" + i+1 + ") - " + cartasEmMaos.get(i).getNumero() + " de " + cartasEmMaos.get(i).getNaipe() + ", valor: " + cartasEmMaos.get(i).getNumero().getValor() + " pontos");
        }
        System.out.println();
    }

    public void adicionaCarta(Carta carta) {
        this.cartasEmMaos.add(carta);
        incrementaPontos(carta);
    }

    private void incrementaPontos(Carta carta){
        this.pontos += carta.getNumero().getValor();
    }

    public int getPontos(){
        return this.pontos;
    }
}
