package br.com.itau;

import java.util.ArrayList;
import java.util.Random;

public class Baralho implements Cartas{

    private ArrayList<Carta> cartas;

    public Baralho(){
        this.cartas = new ArrayList<>();
    }

    public void carregaCartas(){
        carregaBaralhoInteiro();
        //carregaCartasDeUmNaipe(Naipe.COPAS);
        //carregaCartasDeUmNaipe(Naipe.OUROS);
        //carregaCartasDeUmNaipe(Naipe.ESPADAS);
        //carregaCartasDeUmNaipe(Naipe.PAUS);
    }

    public void carregaBaralhoInteiro(){
        this.cartas = new ArrayList<>();
        for (Naipe naipe: Naipe.values()){
            for (Numero numero: Numero.values()) {
                Carta carta = new Carta(numero, naipe);
                cartas.add(carta);
            }
        }
        System.out.println("\n*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("*-*-*-*-*- O Croupier pegou um novo baralho *-*-*-*-*");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
    }

    private void carregaCartasDeUmNaipe(Naipe naipe){
        for(int i = 0; i < Numero.values().length; i++ ){
            Carta cartaNaipe;
            cartaNaipe = new Carta(Numero.values()[i], naipe);
            cartas.add(cartaNaipe);
        }
        System.out.println("*-*-*-*-*- O Croupier pegou um novo baralho, mas somente com as cartas de " + naipe + " -*-*-*-*-*");
    }

    public Carta sorteiaCarta(){
        Random random = new Random();
        int posicaoCartaSorteada = random.nextInt(cartas.size());
        Carta cartaSorteada = new Carta();
        cartaSorteada = cartas.get(posicaoCartaSorteada);
        retiraCartaSorteada(posicaoCartaSorteada);
        System.out.println("\nA carta sorteada foi " + cartaSorteada.getNumero() + " de " + cartaSorteada.getNaipe() + " e vale " + cartaSorteada.getNumero().getValor() + " pontos.");
        return cartaSorteada;
    }

    private void retiraCartaSorteada(int posicaoCartaSorteada){
        cartas.remove(posicaoCartaSorteada);
    }

    @Override
    public void listaCartas(){
        System.out.println("\n--------------INICIO DA LISTAGEM DO BARALHO-----------");
        System.out.println("Atualmente o baralho tem " + cartas.size() + " cartas, listadas abaixo:\n");
        for(int i = 0; i < cartas.size(); i++){
            System.out.println(cartas.get(i).getNumero() + " de " + cartas.get(i).getNaipe() + ", valor: " + cartas.get(i).getNumero().getValor() + " pontos");
        }
        System.out.println("--------------FIM DA LISTAGEM DO BARALHO--------------\n");
    }


    @Override
    public int getQuantidadeCartas(){
        return cartas.size();
    }

    public void embaralhaCartas(){
        ArrayList<Carta> cartasEmbaralhadas = new  ArrayList();
        int totalDeCartas = getQuantidadeCartas();

        for(int i = 0; i < totalDeCartas; i++){
            Random random = new Random();
            int posicaoCartaEmbaralhada = random.nextInt(cartas.size());

            Carta cartaRetirada = new Carta(cartas.get(posicaoCartaEmbaralhada).getNumero(), cartas.get(posicaoCartaEmbaralhada).getNaipe());
            cartasEmbaralhadas.add(cartaRetirada);
            cartas.remove(posicaoCartaEmbaralhada);
        }

        for(int i = 0; i < totalDeCartas; i++){
            Random random = new Random();
            int posicaoCartaRetornada = random.nextInt(cartasEmbaralhadas.size());

            Carta cartaRetornada = new Carta(cartasEmbaralhadas.get(posicaoCartaRetornada).getNumero(), cartasEmbaralhadas.get(posicaoCartaRetornada).getNaipe());
            cartas.add(cartaRetornada);
            cartasEmbaralhadas.remove(posicaoCartaRetornada);
        }

        System.out.println("\n*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("*-*-*-*-*- O Croupier embaralhou as cartas -*-*-*-*-*");
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");

    }
}
