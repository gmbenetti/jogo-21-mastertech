package br.com.itau;

public enum Numero {
    AS(1),
    DOIS(2),
    TRES(3),
    QUATRO(4),
    CINCO(5),
    SEIS(6),
    SETE(7),
    OITO(8),
    NOVE(9),
    DEZ(10),
    J(10),
    Q(10),
    K(10);

    private int valor;

    Numero(int valor){
        this.valor = valor;
    }

    public int getValor(){
        return this.valor;
    }
}
