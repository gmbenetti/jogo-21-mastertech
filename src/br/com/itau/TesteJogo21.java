package br.com.itau;


import java.util.Random;

public class TesteJogo21 {

    public void executaTeste(){
        Numero numero = Numero.AS;
        System.out.println(numero);
        System.out.println(numero.getValor());

        Numero numero2 = Numero.DOIS;
        System.out.println(numero2);
        System.out.println(numero2.getValor());

        //Método de Sorteio direto pelos enums---------------------------------------------
        int sorteioNaipe = new Random().nextInt(Naipe.values().length);
        System.out.println("naipe posicao: " + sorteioNaipe);
        System.out.println("naipe: " + Naipe.values()[sorteioNaipe]);

        int sorteioCarta = new Random().nextInt(Numero.values().length);
        System.out.println("carta posição: " + sorteioCarta);
        System.out.println("carta: " + Numero.values()[sorteioCarta]);
        System.out.println("valor carta: " + Numero.values()[sorteioCarta].getValor() + "\n");
        System.out.println("Carta Sorteada: " + Numero.values()[sorteioCarta] + " de " + Naipe.values()[sorteioNaipe] + " que vale " + Numero.values()[sorteioCarta].getValor() + "\n");
        //-------------------------------------------------------------------------------------

        //Metodo de Sorteio usando o baralho do ArrayList e ramdom
        Baralho baralho = new Baralho();
        baralho.carregaCartas();
        Carta cartaSorteada = new Carta();
        cartaSorteada = baralho.sorteiaCarta();
        System.out.println("A carta sorteada é " + cartaSorteada.getNumero() + " de " + cartaSorteada.getNaipe() + " e vale " +  cartaSorteada.getNumero().getValor() + " pontos.");
    }

}
