package br.com.itau;

import java.util.ArrayList;
import java.util.Scanner;

public class InterfaceJogo {

    private CartasEmMaos cartasEmMaos;
    private Baralho baralho;

    public void executaJogo() {

        cartasEmMaos = new CartasEmMaos();
        baralho = new Baralho();
        baralho.carregaCartas();
        baralho.embaralhaCartas();


        int acao;

        do {
            if (baralho.getQuantidadeCartas() == 0) {
                System.out.println("ACABARAM AS CARTAS DO BARALHO, JOGO FINALIZADO");
                return;
            } else {
                Scanner scanner = new Scanner(System.in);
                System.out.println("\nO que deseja?");
                System.out.println("\n    (0) sortear uma carta?");
                System.out.println("    (1) ver suas cartas e pontuação?");
                System.out.println("    (2) embaralhar?");
                System.out.println("    (3) listar baralho?");
                System.out.print("    (4) ver sua melhor pontuação?      ");
                acao = scanner.nextInt();

                if (acao == 0) {
                    Carta cartaSorteada = new Carta();
                    cartaSorteada = baralho.sorteiaCarta();
                    cartasEmMaos.adicionaCarta(cartaSorteada);
                    int pontos = cartasEmMaos.getPontos();
                    if (pontos == 21){
                        System.out.println("          \\o/ Uau, você atingiu a pontuação máxima. Parabéns!!!");
                        acao = 9;
                    }else if (pontos > 21){
                        System.out.println("          Você   E S T O U R O U   a pontuação. Tente novamente :(");
                        acao = 9;
                    }
                } else if (acao == 1) {
                    cartasEmMaos.listaCartas();
                    int pontos = cartasEmMaos.getPontos();
                    System.out.println("       TOTAL DE PONTOS: " + pontos);
                } else if (acao == 2) {
                    baralho.embaralhaCartas();
                } else if (acao == 3) {
                    baralho.listaCartas();
                } else if (acao == 4) {

                }
            }
        } while (acao != 9);
    }
}
