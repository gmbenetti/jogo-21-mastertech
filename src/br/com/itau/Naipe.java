package br.com.itau;

public enum Naipe {
    OUROS,
    PAUS,
    ESPADAS,
    COPAS
}
