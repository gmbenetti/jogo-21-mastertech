package br.com.itau;

public class Carta {
    private Numero numero;
    private Naipe naipe;


    public Carta(){}

    public Carta(Numero numero, Naipe naipe){
        this.numero = numero;
        this.naipe = naipe;
    }

    public Numero getNumero(){
        return this.numero;
    }

    public Naipe getNaipe(){
        return this.naipe;
    }
}
